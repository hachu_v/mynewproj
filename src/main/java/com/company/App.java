package com.company;
import com.company.demo.Card;
import com.company.demo.Rank;
import com.company.demo.Player;
import java.util.UUID;

public class App {
    public static void main(String[] args) {

        Player player = Player.builder().build();
        player.setPlayerId(UUID.randomUUID().toString());
        player.setPlayerName("John");
        player.setPlayerRank(Rank.LOWER);
        player.setCard(Card.HEARTS);
        System.out.println(player.getDetails());

    }
}

