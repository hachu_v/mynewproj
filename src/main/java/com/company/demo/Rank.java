package com.company.demo;

public enum Rank {
    LOWER("The Rank Is Lower"),
    MEDIUM("The Rank Is medium"),
    HIGHER("The Rank Is Higher");
    private String playerRank;
    private Rank(String rank)
    {
        playerRank=rank;
    }

    public String getPlayerRank() {
        return playerRank;
    }

}
