package com.company.demo;

public enum Card {
    SPREADS(100),
    CLUBS(70),
    HEARTS(90),
    DIAMOND(80);
    private int cardRank;

    private Card(int r) {
        cardRank = r;
    }

    public int getCardRank() {
        return cardRank;
    }

}
